#TODO: Option to delete/keep files on upload/download?

Get-EventSubscriber | Unregister-Event
Get-Job | Stop-Job
Get-Job | Remove-Job



#CODE FOR ADDING A NOTIFYICON. PUT ON HOLD FOR NOW AS NOTIFYICON DOES NOT DISPLAY WHEN PicTransfer IS RUN AS A SERVICE. CREATING THE ICON AND A CONTEXT MENU WORKS
<# $SB = {
    Add-Type -AssemblyName System.Drawing
    Add-Type -AssemblyName System.Windows.Forms

    $MenuItemExit = New-Object -TypeName System.Windows.Forms.MenuItem
    $MenuItemExit.Index = 0
    $MenuItemExit.Text = "Exit"
    $MenuItemExit.Add_Click{[System.Environment]::Exit(0)}


    $ContextMenu = New-Object -TypeName System.Windows.Forms.ContextMenu
    $ContextMenu.MenuItems.Add($MenuItemExit)

    $NotifyIcon = New-Object -TypeName System.Windows.Forms.NotifyIcon
    $NotifyIcon.Visible = $true
    $NotifyIcon.Text = "PicTransfer"
    $NotifyIcon.ContextMenu = $ContextMenu

    $iconBase64 = "AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAABAAADgOAAA4DgAAAAAAAAAAAAAAAAACAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAMAAAAAAAAAADMtKBswKSM9MywlOzcvKDk3Lyg5Ni4oOTIrJD4wKCI/NS4nOTcvKDk3Lyg6Ni4nPTApI0IyKiQ+Ny8oOzcvKDs3Lyg7NCwmPy8oIkI0LSY8Ny8pOzcvKDs2Lig7MSokPzApI0A2Lig7Ny8oOTgyLTk4NDE1KiclHzEtKhw6NjMiP0FExEpaavNKXW/zTF9x801hc/NNYXPzTWFz80xfcfNLXW/zTWBy801gcvRNX3H0TF9x9EpcbvVLXG30TF9x9E1fcfRNX3H0TF9x9Updb/RMX3H0TWFz9E5idPROYnbzTF9x9Etdb/RNYXPzQ0pR8jo1Mpg4NDE1Mi8sPjczMFJGUV3+YYy3/2ORvv9ikr//ZZbE/2aYx/9mmMb/ZpjG/2SVwv9jk8D/ZpfF/2aYxv9mmMb/ZpfF/2OTwP9klcL/ZpjG/2aZx/9hjLT/WHmY/1Z2lf9Yepr/WXub/1l7m/9Zepr/Wn+i/2OUwv9SbIT9OTQvWTk1MwAAAAACAAAABEdSX/9hjLj/Yo+8/2KQvf9ikr7/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZZfF/2OTv/9klcL/ZpnH/12EqP9RaH//UGd9/05lev9QZ33/UWl//1Bofv9XdpL/Y5TB/1Bqgv44My9aOTYzAAAAAAAAAAABRlFe/2GMt/9ij7z/Yo+8/2KQvf9ikb7/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZZfF/2OTv/9klcP/XIKl/09mev9PZnv/TmR4/01idv9PZXr/T2V6/1Z0kP9mmMb/UWuD/jQvK15JREAAAAAAAAAAAABFUFz/Xomz/2KPvP9ij7z/Yo+8/2KQvP9ikb3/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZZfF/2OTwP9firL/Wn2e/1p+n/9bf6H/Wn6f/1d6mv9Ze5v/Xoar/2aZx/9Tbob+NTAsYAAAAAMAAAABAAAAAEZSXv9eiLL/X4u3/2KPvP9ij7z/Yo+8/2KPvP9ikb3/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZZfF/2SVw/9WdpT/XIOm/2eby/9nmsn/YpC7/1Bqg/9klcH/Z5rJ/1Nuhv45My9bAAAAAgAAAAcAAAABR1Jf/2CMt/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2KPvP9ikL3/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/YpC7/05kef9UcYv/ZJO//2WWw/9ehar/SVlo/1yFq/9llsT/U26H/jk0MFpDPjsAAAAABgAAAAZHUl//YYy4/2KPvP9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GPu/9hkLz/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aZx/9WdI//QUZL/0FHTv9Ye5v/Y5C6/0lYZ/8/QkX/Slxs/2GPuv9SbIX+OTQwWjo2MwAAAAABAAAAA0dSXv9hjLj/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GOu/9hkLz/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZpjF/2STvv9PZXn/UmyE/2KRvP9klcP/XIGj/0paaf9ehqv/ZZjG/1Bpgf43Mi5bOTUyAAAAAAAAAAAARVFd/2CLtv9ij7z/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GOu/9hj7z/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZpjG/2aXxf9mmMb/ZpfF/2OTv/9klcL/ZZfE/2aZx/9nmsj/UmyF/jMvK2CroJcAAAAAAAAAAABFUFz/XYix/2GOu/9ij7z/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GOuv9hj7v/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZpfF/2aYxv9mmMb/ZZfF/2OTv/9klcL/ZpjG/2eayP9Tbob+NzIuXgAAAAUAAAADAAAAAEdSX/9fibT/X4q1/2GOuv9ij7z/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GOuv9hj7v/ZZbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZZfF/2OTv/9klML/Z5rI/1Nuhv45NDBa////AAAAAAkAAAADR1Jf/2GMuP9gjLj/X4q1/2GOuv9ij7z/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GOuv9hjrr/ZJbD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZZfF/2OTv/9llsT/U22G/jk0MFo6NjMAAAAAAwAAAAZHUl//YYy4/2KPvP9gjLj/X4q1/2GOuv9ij7z/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GOuv9gjrr/ZJXD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZZfF/2OUwv9RaoL+OTQwWjo2MwAAAAAAAAAAAkZSXv9hjLj/Yo+8/2KPvP9gjLj/X4q1/2GOuv9ij7z/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GNuv9gjrn/ZJXD/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9mmMb/ZpnH/1Bqgv41MCxcPTk2AAAAAAAAAAAARVBc/1+Ktf9ij7z/Yo+8/2KPvP9gjLj/X4q1/2GOuv9ij7z/Yo+8/2KPu/9fi7b/X4u3/2KPvP9ij7z/Yo+8/2GNuv9gjbn/ZJXC/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2aYxv9nmsj/U22G/jQvK2EAAAABAAAAAAAAAABGUV3/XYex/2CNuf9ij7z/Yo+8/2KPvP9gjLj/X4q1/2GOuv9ij7z/YpC9/2KPvf9fjLj/YIy4/2KQvf9ikL3/YpC+/2GOu/9gjbn/ZJXC/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZZfF/2eayP9Tbob+ODMvXAAAAAQAAAAGAAAAAEdSX/9gi7b/X4q1/2CMuP9ij7z/Yo+8/2KPvP9gjLj/X4q1/2GNuf9cg6r/WX2i/1l9of9WeZz/V3qe/1l9ov9ZfaL/W3+l/1+KtP9gjbj/ZJXC/2aYxv9mmMb/ZpjG/2SUwf9jk8D/ZpnH/1Nuhv45NDBaWFJOAAAAAAgAAAAFR1Jf/2GMuP9hjrv/X4q1/2CMuP9ij7z/Yo+8/2KPvP9gjbn/W4Oq/01ZY/9od33/anqC/2p6gf9nd37/ZnZ9/2p7gv9WX2T/VnaV/2GPvP9gjLj/ZJXC/2aYxv9mmMb/ZpjG/2SUwf9klML/Um2F/jk0MFo5NjMAAAAAAQAAAAVHUl//YYy4/2KPvP9hjrr/X4q1/2CMuP9ij7z/Yo+8/2KPvf9bgqr/W2lx/7Pb6f+75vb/u+X1/7rl9P+13u3/uOLx/36Tmv9UcI7/Y5G+/2GNuv9fjLf/ZJTB/2aYxv9mmMb/ZpjG/2SWxP9QaYH+ODMvWjk1MgAAAAAAAAAAAEZRXf9gjLf/Yo+8/2KPvP9hjrr/X4q1/2CMuP9ij7z/YpC9/12Frf9canP/r9bk/7vm9v+85/f/vOf3/7vm9v+44/L/e5CX/1Rxjv9jkb7/Yo+8/2GNuv9fjLf/ZJTB/2aYxv9mmMb/Z5nI/1FrhP40LytfVlBMAAAAAAAAAAAARVBc/16Isv9ij7z/Yo+8/2KPvP9hjrr/X4q1/2CMuP9ikL3/XYWt/15tdv+y2uj/tt/v/7rl9f+85/f/vOf3/73p+f98kZj/Um6L/2KQvv9ij7z/Yo+8/2GNuv9fi7f/ZJTB/2aYxv9nmsj/U26G/jUwLV8AAAAEAAAAAQAAAABHUl7/Xoiy/1+Ltv9ij7v/Yo+8/2KPvP9hjrr/X4q1/2CNuf9dha3/Xm12/7be7f+54/P/td/u/7rl9f+85/f/vur6/3+UnP9RbYn/YI25/2KPvP9ij7z/Yo+8/2GNuv9fi7b/Y5PA/2eayf9Tbob+OTQvWgAAAAEAAAAIAAAAAUdSX/9hjLf/X4u3/1+Ltv9ij7v/Yo+8/2KPvP9hjrr/X4u2/1uCqv9ebXb/tt7t/7zo+P+44vL/td/u/7rl9f++6vr/f5Wc/1Nwjf9gjLj/X4u3/2KPvP9ij7z/Yo+8/2GNuv9fi7b/ZJXC/1Nuhv45NDBaPjo3AAAAAAUAAAAGR1Je/2GMt/9ij7z/X4u2/1+Ltv9ijrv/Yo+8/2KPvP9hjrv/WYCn/11rdP+13u3/vOf4/7zn9/+44vL/td/u/7zo+P9/lZz/VHCO/2KQvf9fi7b/X4u3/2KPu/9ij7z/Yo+8/2GNuf9fjLj/UGmA/jk0MFo6NjMAAAAAAAAAAANIVGD/Y5C7/2STwP9kkr//Yo+6/2GOuv9kkr//ZJPA/2WTwP9eh6//XWpz/7jc6f/C6fj/wun4/8Lp+P++5PP/vePy/4CUm/9Vc5H/ZZTC/2SSv/9hjrr/Yo+6/2SSv/9kk8D/ZJPA/2SSwP9PZ37+NzIuWzo2MwAAAAAAAAAAAEtYZP5ypdH/davZ/3Wr2f91q9n/cqfT/3Km0/91qtj/dqza/26eyP9qc3r/1+Po/+Px+P/n9fz/5/X8/+f1/P/l8/r/kJaZ/2CDof92rdz/davZ/3Wr2P9yptP/cqbT/3Wr2f91q9n/dq3c/1p2jv4zLipg////AAAAAAAAAAAAQENGxFBkdPNVa33zVWt+81VrfvNVa33zU2h581Noe/NVa37zUmZ281BRU/SAgYH0gYKD9ISGhvWGiIj0hoiI9IeJifRgYF/0SVdi81VsfvNVa37zVWt+81VrffNSZ3nzU2l781VrfvNVa370SFFZ2TUwLDYAAAAGAAAABAAAAAA3MCsYMiojOC4mIUAzKyU7NS0mOTUtJjk1LSY5LychPS8oIj81LSc6NjEuOi4pJTsrJiM8JyIfQiolIj8tKCQ7LSgkOzMvKzsyLCc8LichQDQrJTs1LSY5NS0mOTUsJjkvJyE+MCgiPzUsJjo2LigkPjw6AgAAAAMAAAAIAAAAAwAAAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAACAAAAAQAAAAAAAAAAAAAAAAAAAAIAAAAEOeec8wAAAAAAAAAAAAAABAAAAAYAAAAHAAAAAQAAAAAAAAAEAAAABAAAAAcAAAAHAAAAAQAAAAQAAAAEAAAABgAAAAcAAAADAAAAAQAAAAQAAAAEAAAABwAAAAcAAAABAAAAAAAAAAQAAAAGAAAABwAAAAcAAAABAAAAAM8955w="
    $iconBytes = [Convert]::FromBase64String($iconBase64)
    $stream = New-Object IO.MemoryStream($iconBytes, 0, $iconBytes.Length)
    $stream.Write($iconBytes, 0, $iconBytes.Length);
    #$iconImage = [System.Drawing.Image]::FromStream($stream, $true)
    $NotifyIcon.Icon = [System.Drawing.Icon]::FromHandle((New-Object System.Drawing.Bitmap -Argument $stream).GetHIcon())

}

Invoke-Command -ScriptBlock $SB #>


function Write-Log([string] $Text)
{
    "$(Get-Date -Format "dd/MM/yyyy HH:mm") - $Text" | Out-File -FilePath $LogFile -Append
    Write-Host "$(Get-Date -Format "dd/MM/yyyy HH:mm") - $Text"
}

$Config = Get-Content -Path "$PSScriptRoot\config.json" | ConvertFrom-Json

$AccessKeyUpload = $Config.AwsKeyUpload
$SecretKeyUpload = $Config.AwsSecretKeyUpload
$RegionUpload = $Config.AwsRegionUpload
$S3UploadBucket = $Config.S3UploadBucket
$UploadFolderPath = $Config.UploadFolderPath
$DeleteLocalFilesAfterUpload = $Config.DeleteLocalFilesAfterUpload
$AccessKeyDownload = $Config.AwsKeyDownload
$SecretKeyDownload = $Config.AwsSecretKeyDownload
$RegionDownload = $Config.AwsRegionDownload
$S3DownloadBucket = $Config.S3DownloadBucket
$DownloadFolderPath = $Config.DownloadFolderPath
$S3DownlodBucketCheckInterval = $Config.S3DownloadBucketCheckInterval
$LogFile = "$PSScriptroot\Logs\PicTransfer - $(Get-Date -Format "yyyy-MM-dd").log"
$LogCheckIntervalMinutes = 1440
$LogRetentionDays = 30

function Remove-OldLogs()
{
    Write-Log -Text "Checking for old log files"
    $LogFiles = Get-ChildItem -Path "$PSScriptRoot\Logs"

    foreach ($File in $LogFiles)
    {
        if ($File.CreationTime -lt $( $(Get-Date).AddDays(-$($LogRetentionDays)) ))
        {
            Remove-Item -Path $File.FullName

            Write-Log -Text "Removed old logfile $($File.Name) as it was more than $($LogRetentionDays.ToString()) days old"
        }
    }
}


#This and child functions MUST be in the global scope, otherwise the event subscriber will not work
function global:UploadMode()
{
    #Sanity check the config
    if ([System.String]::IsNullOrWhiteSpace($Config.AwsKeyUpload) -eq $true)
    {
        Write-Log -Text "The upload AWS access key seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.AwsSecretKeyUpload) -eq $true)
    {
        Write-Log -Text "The upload AWS secret key seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.AwsRegionUpload) -eq $true)
    {
        Write-Log -Text "The upload AWS region key seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.S3UploadBucket) -eq $true)
    {
        Write-Log -Text "The S3 upload bucket seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.UploadFolderPath) -eq $true)
    {
        Write-Log -Text "The upload folder path seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }
    function global:S3Upload([System.Object]$EventObj)
    {
        $Props = @{
            "BucketName" = $S3UploadBucket
            "File" = $EventObj.SourceEventArgs.FullPath
            "AccessKey" = $AccessKeyUpload
            "SecretKey" = $SecretKeyUpload
            "Region" = $RegionUpload
            "Key" = "PicTransfer\$env:COMPUTERNAME\$($EventObj.SourceEventArgs.Name)"
        }

        try
        {
            Write-S3Object @Props
            Write-Log -Text "Uploaded '$($EventObj.SourceEventArgs.FullPath)' to S3"

            if ($DeleteLocalFilesAfterUpload -eq "Yes")
            {
                try{
                    Remove-Item -Path $EventObj.SourceEventArgs.FullPath
                    Write-Log -Text "Deleted local file '$($EventObj.SourceEventArgs.FullPath)'"
                }
                catch
                {
                    Write-Log -Text "Failed to delete '$($EventObj.SourceEventArgs.FullPath)'. Continuing anyway..."
                }
            }
        }
        catch
        {
            Write-Log -Text "Failed to upload '$($EventObj.SourceEventArgs.FullPath)'. Continuing anyway..."
        }
    }


    $UploadFileWatcher = New-Object -TypeName System.IO.FileSystemWatcher
    $UploadFileWatcher.Path = $UploadFolderPath

    #Upload when a new file was created
    Register-ObjectEvent -InputObject $UploadFileWatcher -EventName Created -SourceIdentifier File.Created -Action {S3Upload($event)}
    
    #Upload when a file was modified as PSI replaces some files, which counts as modification. THIS EVENT MAY FIRE MULTIPLE TIMES. IGNORE THE DUPLICATES
    Register-ObjectEvent -InputObject $UploadFileWatcher -EventName Changed -SourceIdentifier File.Modified -Action {S3Upload($event)}

    


}
function global:DownloadMode()
{
    #Sanity check the config
    if ([System.String]::IsNullOrWhiteSpace($Config.AwsKeyDownload) -eq $true)
    {
        Write-Log -Text "The download AWS access key seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.AwsSecretKeyDownload) -eq $true)
    {
        Write-Log -Text "The download AWS secret key seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.AwsRegionDownload) -eq $true)
    {
        Write-Log -Text "The download AWS region key seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.S3DownloadBucket) -eq $true)
    {
        Write-Log -Text "The S3 download bucket seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.DownloadFolderPath) -eq $true)
    {
        Write-Log -Text "The download folder path seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }

    if ([System.String]::IsNullOrWhiteSpace($Config.S3DownloadBucketCheckInterval) -eq $true)
    {
        Write-Log -Text "The S3 download bucket check interval seems to be misconfigured, please check. Exiting in 5 seconds"

        Start-Sleep -Seconds 5

        Exit 0
    }




    #Checks files in S3 and adds them to $ObservableCollection if they don't exist there yet
    function global:S3FileCheck()
    {
        Write-Log -Text "Checking S3 download bucket for new files"
        $Props = @{
            "BucketName" = $S3DownloadBucket
            "Prefix" = "PicTransfer/$env:COMPUTERNAME"
            "AccessKey" = $AccessKeyDownload
            "SecretKey" = $SecretKeyDownload
            "Region" = $RegionDownload
        }

        $S3Files = Get-S3Object @Props
        foreach ($File in $S3Files)
        {
            if ($ObservableCollection -notcontains $File.Key)
            {
                Write-Log -Text "Adding $($File.Key) to the observable collection"
                $ObservableCollection.Add($File.Key)
            }
        }
    }

    function global:S3FileDownload([System.Object] $EventObj)
    {
        switch ($EventObj.SourceEventArgs.Action)
        {
            "Add" {
                Write-Log -Text "$($EventObj.SourceEventArgs.NewItems) was added to the S3 upload bucket. Downloading..."
                $FileName = $($EventObj.SourceEventArgs.NewItems).Split("/")[2]
                #Donwload stuff
                #'using' makes the local variables available to the jobs
                $DownloadJob = Start-Job -ScriptBlock {Read-S3Object -BucketName $using:Config.S3DownloadBucket -Key "$($using:EventObj.SourceEventArgs.NewItems)" -File "$($using:Config.DownloadFolderPath)\$using:FileName" -AccessKey $using:Config.AwsKeyDownload -SecretKey $using:Config.AwsSecretKeyDownload -Region $using:Config.AwsRegionDownload}
                #$DownloadJob = Start-Job -ScriptBlock {Get-S3Object @DownloadProps}

                Wait-Job -Job $DownloadJob 
                
                Write-Log -Text "Download complete. Removing $($EventObj.SourceEventArgs.NewItems) from the S3 download bucket"
                $RemovalJob = Start-Job -ScriptBlock {Remove-S3Object -BucketName $using:Config.S3DownloadBucket -Key "$($using:EventObj.SourceEventArgs.NewItems)" -AccessKey $using:Config.AwsKeyDownload -SecretKey $using:Config.AwsSecretKeyDownload -Region $using:Config.AwsRegionDownload -Force}
                #$RemovalJob = Start-Job -ScriptBlock {Remove-S3Object @RemovalProps}
                Wait-Job -Job $RemovalJob  
                

                $ObservableCollection.Remove($EventObj.SourceEventArgs.NewItems)
            }
            "Remove" {Write-Log -Text "Removed $($EventObj.SourceEventArgs.OldItems) from the observable collection"}
            default {Write-Host "This is default"}
        }
    }

    #Runs the file check once, when the script gets started
    S3Filecheck
}

    #Holds the names of files that still need to be downloaded
    $global:ObservableCollection = New-Object System.Collections.ObjectModel.ObservableCollection[string]

    Register-ObjectEvent -InputObject $ObservableCollection -EventName CollectionChanged -Action {S3FileDownload($Event)}


    $Timer = New-Object -TypeName timers.timer
    $Timer.Interval = [int]$S3DownlodBucketCheckInterval * 1000

    Register-ObjectEvent -InputObject $Timer -EventName Elapsed -SourceIdentifier Timer.Output -Action {S3FileCheck}

    $Timer.Enabled = $true


    $LogTimer = New-Object -TypeName timers.timer
    $LogTimer.Interval = [int]$LogCheckIntervalMinutes * 60000

    Register-ObjectEvent -InputObject $LogTimer -EventName Elapsed -SourceIdentifier LogTimer.Elapsed -Action{Remove-OldLogs}

    $LogTimer.Enabled

#Install necessary modules
if ($(Get-InstalledModule -Name "AWS.Tools.S3" -ErrorAction SilentlyContinue) -eq $None)
{
    Write-Log -Text "AWS S3 PowerShell module not found. Insalling now..."

    try
    {
        Install-Module -Repository psGallery -Name "AWS.Tools.S3" -Scope AllUsers

        Write-Log -Text "Installed AWS S3 PowerShell module"
    }
    catch
    {
        Install-Module -Repository psGallery -Name "AWS.Tools.S3" -Scope CurrentUser

        Write-Log -Text "Installed AWS S3 PowerShell module"
    }

    
}

#Import necessary modules
try
{
    Import-Module -Name "AWS.Tools.S3"
    Write-Log -Text "Successfully imported AWS.Tools.S3"
}
catch
{
    Write-Log -Text "Failed to import AWS.Tools.S3. Please check the module installed correctly. Exiting..."

    Exit 1
}

switch ($Config.Mode)
{
    "Upload" {UploadMode}
    "Download" {DownloadMode}
    "UploadAndDownload" {UploadMode; DownloadMode}
}

#Check for old logs once
Remove-OldLogs

while ($true)
{
    Wait-Event
}