# Hide PowerShell Console
Add-Type -Name Window -Namespace Console -MemberDefinition '
[DllImport("Kernel32.dll")]
public static extern IntPtr GetConsoleWindow();
[DllImport("user32.dll")]
public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);
'
$consolePtr = [Console.Window]::GetConsoleWindow()
[Console.Window]::ShowWindow($consolePtr, 0)

Get-EventSubscriber | Unregister-Event



function LoadConfig()
{
    $Config = Get-Content -Path "$PSScriptRoot\config.json" | ConvertFrom-Json

    $TextBoxAwsKeyUpload.Text = $Config.AwsKeyUpload
    $TextBoxAwsSecretKeyUpload.Text = $Config.AwsSecretKeyUpload
    $TextBoxRegionUpload.Text = $Config.AwsRegionUpload
    $TextBoxUploadBucketName.Text = $Config.S3UploadBucket
    $TextBoxUploadFolder.Text = $Config.UploadFolderPath
    $TextBoxAwsKeyDownload.Text = $Config.AwsKeyDownload
    $TextBoxAwsSecretKeyDownload.Text = $Config.AwsSecretKeyDownload
    $TextBoxRegionDownload.Text = $Config.AwsRegionDownload
    $TextBoxDownloadBucketName.Text = $Config.S3DownloadBucket
    $TextBoxDownloadFolder.Text = $Config.DownloadFolderPath
    $TextBoxS3DownloadBucketCheckInterval.Text = $Config.S3DownloadBucketCheckInterval

    switch ($Config.Mode)
    {
        "Upload" {$RadioButtonUpload.Checked = $true}
        "Download" {$RadioButtonDownload.Checked = $true}
        "UploadAndDownload" {$RadioButtonUploadAndDownload.Checked = $true}
        default {$none}
    }

    switch ($Config.DeleteLocalFilesAfterUpload)
    {
        "Yes" {$RadioButtonDeleteLocalFilesYes.Checked = $true}
        "No" {$RadioButtonDeleteLocalFilesNo.Checked = $true}
        default {$none}
    }

}

function SaveConfig()
{
    $Config = Get-Content -Path "$PSScriptRoot\config.json" | ConvertFrom-Json

    $Config.AwsKeyUpload = $TextBoxAwsKeyUpload.Text
    $Config.AwsSecretKeyUpload = $TextBoxAwsSecretKeyUpload.Text
    $Config.AwsRegionUpload = $TextBoxRegionUpload.Text
    $Config.S3UploadBucket = $TextBoxUploadBucketName.Text
    $Config.UploadFolderPath = $TextBoxUploadFolder.Text
    $Config.AwsKeyDownload = $TextBoxAwsKeyDownload.Text
    $Config.AwsSecretKeyDownload = $TextBoxAwsSecretKeyDownload.Text
    $Config.AwsRegionDownload = $TextBoxRegionDownload.Text
    $Config.S3DownloadBucket = $TextBoxDownloadBucketName.Text
    $Config.DownloadFolderPath = $TextBoxDownloadFolder.Text
    $Config.S3DownloadBucketCheckInterval = $TextBoxS3DownloadBucketCheckInterval.Text

    #Check which radio button was checked
    foreach ($RadioButton in $TableLayoutPanelRadioButtons1.Controls)
    {
        if ($RadioButton.Checked -eq $true)
        {
            switch ($RadioButton.Text)
            {
                "Upload" {$Config.Mode = "Upload"; break}
                "Download" {$Config.Mode = "Download"; break}
                "Upload and download" {$Config.Mode = "UploadAndDownload"; break}
            }
        }
    }

    foreach ($RadioButton in $TableLayoutpanelRadioButtonsDeleteLocalFiles.Controls)
    {
        if ($RadioButton.Checked -eq $true)
        {
            switch ($RadioButton.Text)
            {
                "Yes" {$Config.DeleteLocalFilesAfterUpload = "Yes"; break}
                "No" {$Config.DeleteLocalFilesAfterUpload = "No"; break}
            }
        }
    }


    $Config | ConvertTo-Json | Out-File -FilePath "$PSScriptRoot\config.json"

    $ToolStripStatusLabel.Text = ""

    $ServiceExists = Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue
    

    if ($ServiceExists -ne $none)
    {
        $ServiceRunning = $(Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue).Status.ToString()

        if ($ServiceRunning -eq "Running")
        {
            $ToolStripStatusLabel.Text = "Detected PicTransfer serivce is running. Stopping service. "

            Start-Process powershell.exe -ArgumentList "-command `"Stop-Service -Name `"PicsolvePicTransfer`"`" -ErrorAction SilentlyContinue" -Verb RunAs -Wait -WindowStyle Hidden

            $ServiceRunning = $(Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue).Status.ToString()

            if ($ServiceRunning -eq "Stopped")
            {
                Write-Host $ToolStripStatusLabel.Text + "Restarting service. "

                $ToolStripStatusLabel.Text = $ToolStripStatusLabel.Text + "Restarting service. "

                Start-Process powershell.exe -ArgumentList "-command `"Start-Service -Name `"PicsolvePicTransfer`"`" -ErrorAction SilentlyContinue" -Verb RunAs -Wait -WindowStyle Hidden

                $ServiceRunning = $(Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue).Status.ToString()

                if ($ServiceRunning -eq "Running")
                {
                    $ToolStripStatusLabel.Text = $ToolStripStatusLabel.Text + "Done. Config applied."
                }
            }
        }
        else
        {
            $ToolStripStatusLabel.Text = "Config saved. PicTransfer service is installed, but not started. If PicTransfer is running please restart it to apply the new config."
        }
    }
    else
    {
        $ToolStripStatusLabel.Text = "Config saved. PicTransfer service not detected. If PicTransfer is running please restart it to apply the new config."
    }
}

function ButtonUploadFolderChooser_Click()
{
    $FolderBrowserUploadFolder.ShowDialog()

    $TextBoxUploadFolder.Text = $FolderBrowserUploadFolder.SelectedPath
}

function ButtonDownloadFolderChooser_Click()
{
    $FolderBrowserDownloadFolder.ShowDialog()

    $TextBoxDownloadFolder.Text = $FolderBrowserDownloadFolder.SelectedPath
}

function ButtonInstallAsService_Click()
{
    $Service = Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue

    #Remove the PicTransfer service
    if ($Service -ne $none)
    {
        Start-Process powershell.exe -ArgumentList "-command `"Stop-Service -Name `"PicsolvePicTransfer`"`" -ErrorAction SilentlyContinue" -Verb RunAs -Wait -WindowStyle Hidden

        Start-Process "$PSScriptRoot\Service\PicsolvePicTransfer.exe" -ArgumentList "uninstall" -Verb RunAs -Wait -WindowStyle Hidden
        Start-Sleep -Milliseconds 2000

        #Only change the button text if the service was really removed
        CheckServiceInstalled
        
    }
    #Install the PicTransfer Service
    else
    {
        Start-Process "$PSScriptRoot\Service\PicsolvePicTransfer.exe" -ArgumentList "install" -Verb RunAs -Wait -WindowStyle Hidden

        #Only change the button text if the servicce was really installed
        CheckServiceInstalled

    }
}

function ButtonStartService_Click()
{

    $ServiceCheck = Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue
    if ($ServiceCheck.Status.ToString() -eq "Running")
    {
        #Start-Process is needed to run stop-service as admin
        Start-Process powershell.exe -ArgumentList "-command `"Stop-Service -Name `"PicsolvePicTransfer`"`"" -Verb RunAs -Wait -WindowStyle Hidden

        Start-Sleep -Seconds 2

        CheckServiceRunning
    }
    else 
    {
        Start-Process powershell.exe -ArgumentList "-command `"Start-Service -Name `"PicsolvePicTransfer`"`"" -Verb RunAs -Wait -WindowStyle Hidden

        Start-Sleep -Seconds 2

        CheckServiceRunning
    }
}

function CheckServiceInstalled()
{
    $ServiceCheck = Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue

    if ($ServiceCheck -ne $none)
    {
        $LabelInstalledAsServiceIndicator.Text = "Yes"
        $LabelInstalledAsServiceIndicator.ForeColor = [System.Drawing.Color]::Green

        $ButtonInstallAsService.Text = "Remove PicTransfer as a service"

        $ButtonStartService.Visible = $true

        return $true
    }
    else 
    {
        $LabelInstalledAsServiceIndicator.Text = "No"
        $LabelInstalledAsServiceIndicator.ForeColor = [System.Drawing.Color]::Red

        $ButtonInstallAsService.Text = "Install PicTransfer as a service"

        $ButtonStartService.Visible = $false

        CheckServiceRunning

        return $false
    }
}

function CheckServiceRunning()
{
    $Service = Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue

    if ($Service -ne $none){

        $ServiceCheck = Get-Service -Name "PicsolvePicTransfer" -ErrorAction SilentlyContinue
        if ($ServiceCheck.Status.ToString() -eq "Running")
        {
            $LabelServiceRunningIndicator.Text = "Yes"
            $LabelServiceRunningIndicator.ForeColor = [System.Drawing.Color]::Green

            $ButtonStartService.Text = "Stop PicTransfer Service"

            return $true
        }
        else 
        {
            $LabelServiceRunningIndicator.Text = "No"
            $LabelServiceRunningIndicator.ForeColor = [System.Drawing.Color]::Red

            $ButtonStartService.Text = "Start PicTransfer service"
    
            return $false
        }
    }
    else 
    {
        $LabelServiceRunningIndicator.Text = "No"
        $LabelServiceRunningIndicator.ForeColor = [System.Drawing.Color]::Red

        $ButtonStartService.Text = "Start PicTransfer service"

        return $false
    }
}



Add-Type -AssemblyName System.Windows.Forms
$Form = New-Object -TypeName System.Windows.Forms.Form
$Form.Size = [System.Drawing.Size]::new(800, 200)
$Form.Text = "PicTransfer Configurator"
$Form.AutoSize = $true

#Create and configure individual controls
$TableLayoutPanel1 = New-Object -TypeName System.Windows.Forms.TableLayoutPanel
$TableLayoutPanel1.ColumnCount = 2
$TableLayoutPanel1.BorderStyle = [System.Windows.Forms.BorderStyle]::FixedSingle
$TableLayoutPanel1.CellBorderStyle = [System.Windows.Forms.TableLayoutPanelCellBorderStyle]::Single
$TableLayoutPanel1.AutoSize = $true
$TableLayoutPanel1.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
$TableLayoutPanel1.Dock = [System.Windows.Forms.DockStyle]::Fill
foreach ($RowStyle in $TableLayoutPanel1.RowStyles)
{
    $RowStyle.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
}

foreach ($ColumnStyle in $TableLayoutPanel1.ColumnStyles)
{
    $ColumnStyle.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
}

$LabelAwsKeyUpload = New-Object -TypeName System.Windows.Forms.Label
$LabelAwsKeyUpload.Text = "Upload AWS Key"
$TextBoxAwsKeyUpload = New-Object -TypeName System.Windows.Forms.TextBox

$LabelAwsSecretKeyUpload = New-Object -TypeName System.Windows.Forms.Label
$LabelAwsSecretKeyUpload.Text = "Upload AWS Secret Key"
$TextBoxAwsSecretKeyUpload = New-Object -TypeName System.Windows.Forms.TextBox

$LabelRegionUpload = New-Object -TypeName System.Windows.Forms.Label
$LabelRegionUpload.Text = "AWS Upload Bucket Region"
$TextBoxRegionUpload = New-Object -TypeName System.Windows.Forms.TextBox

$LabelUploadBucketName = New-Object -TypeName System.Windows.Forms.Label
$LabelUploadBucketName.Text = "S3 Upload Bucket"
$TextBoxUploadBucketName = New-Object -TypeName System.Windows.Forms.TextBox

$LabelAwsKeyDownload = New-Object -TypeName System.Windows.Forms.Label
$LabelAwsKeyDownload.Text = "Download AWS Key"
$TextBoxAwsKeyDownload = New-Object -TypeName System.Windows.Forms.TextBox

$LabelAwsSecretKeyDownload = New-Object -TypeName System.Windows.Forms.Label
$LabelAwsSecretKeyDownload.Text = "Download AWS Secret Key"
$TextBoxAwsSecretKeyDownload = New-Object -TypeName System.Windows.Forms.TextBox

$LabelRegionDownload = New-Object -TypeName System.Windows.Forms.Label
$LabelRegionDownload.Text = "AWS Download Bucket Region"
$TextBoxRegionDownload = New-Object -TypeName System.Windows.Forms.TextBox

$LabelDownloadBucketName = New-Object -TypeName System.Windows.Forms.Label
$LabelDownloadBucketName.Text = "S3 Download Bucket"
$TextBoxDownloadBucketName = New-Object -TypeName System.Windows.Forms.TextBox

$LabelMode = New-Object -TypeName System.Windows.Forms.Label
$LabelMode.Text = "Mode"
$TableLayoutPanelRadioButtons1 = New-Object -TypeName System.Windows.Forms.TableLayoutPanel
$TableLayoutPanelRadioButtons1.ColumnCount = 3
$TableLayoutPanelRadioButtons1.AutoSize = $true
$TableLayoutPanelRadioButtons1.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
$RadioButtonUpload = New-Object -TypeName System.Windows.Forms.RadioButton
$RadioButtonUpload.Text = "Upload"
$RadioButtonDownload = New-Object -TypeName System.Windows.Forms.RadioButton
$RadioButtonDownload.Text = "Download"
$RadioButtonUploadAndDownload = New-Object -TypeName System.Windows.Forms.RadioButton
$RadioButtonUploadAndDownload.Text = "Upload and download"
$TableLayoutPanelRadioButtons1.Controls.Add($RadioButtonUpload, 0, 0)
$TableLayoutPanelRadioButtons1.Controls.Add($RadioButtonDownload, 1, 0)
$TableLayoutPanelRadioButtons1.Controls.Add($RadioButtonUploadAndDownload, 2, 0)


$LabelUploadFolder = New-Object -TypeName System.Windows.Forms.Label
$LabelUploadFolder.Text = "Upload Folder Path"
$TextBoxUploadFolder = New-Object -TypeName System.Windows.Forms.TextBox
$TextBoxUploadFolder.Dock = [System.Windows.Forms.DockStyle]::Fill
$ButtonUploadFolder = New-Object -TypeName System.Windows.Forms.Button
$ButtonUploadFolder.Text = "..."
$ButtonUploadFolder.AutoSize = $true
$ButtonUploadFolder.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
$FolderBrowserUploadFolder = New-Object -TypeName System.Windows.Forms.FolderBrowserDialog
$ButtonUploadFolder.Add_Click({ButtonUploadFolderChooser_Click})
$TableLayoutPanelUploadFolderSelector = New-Object -TypeName System.Windows.Forms.TableLayoutPanel
$TableLayoutPanelUploadFolderSelector.ColumnCount = 2
$TableLayoutPanelUploadFolderSelector.AutoSize = $true
$TableLayoutPanelUploadFolderSelector.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
$TableLayoutPanelUploadFolderSelector.Dock = [System.Windows.Forms.DockStyle]::Fill
$TableLayoutPanelUploadFolderSelector.Controls.Add($TextBoxUploadFolder, 0, 0)
$TableLayoutPanelUploadFolderSelector.Controls.Add($ButtonUploadFolder, 1, 0)
$ColumnStyle = [System.Windows.Forms.ColumnStyle]::new([System.Windows.Forms.SizeType]::Percent)
$ColumnStyle.Width = 90
$TableLayoutPanelUploadFolderSelector.ColumnStyles.Add($ColumnStyle)
$RowStyle = [System.Windows.Forms.RowStyle]::new([System.Windows.Forms.SizeType]::Absolute)

$LabelUploadDeleteLocalFiles = New-Object -TypeName System.Windows.Forms.Label
$LabelUploadDeleteLocalFiles.Text = "Delete Local Files After Upload"

$RadioButtonDeleteLocalFilesYes = New-Object -TypeName System.Windows.Forms.RadioButton
$RadioButtonDeleteLocalFilesYes.Text = "Yes"
$RadioButtonDeleteLocalFilesYes.AutoSize = $true

$RadioButtonDeleteLocalFilesNo = New-Object -TypeName System.Windows.Forms.RadioButton
$RadioButtonDeleteLocalFilesNo.Text = "No"
$RadioButtonDeleteLocalFilesNo.AutoSize = $true

$TableLayoutpanelRadioButtonsDeleteLocalFiles = New-Object -TypeName System.Windows.Forms.TableLayoutPanel
$TableLayoutpanelRadioButtonsDeleteLocalFiles.ColumnCount = 2
$TableLayoutpanelRadioButtonsDeleteLocalFiles.AutoSize = $true
$TableLayoutpanelRadioButtonsDeleteLocalFiles.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
$TableLayoutpanelRadioButtonsDeleteLocalFiles.Controls.Add($RadioButtonDeleteLocalFilesYes)
$TableLayoutpanelRadioButtonsDeleteLocalFiles.Controls.Add($RadioButtonDeleteLocalFilesNo)


$LabelDownloadFolder = New-Object -TypeName System.Windows.Forms.Label
$LabelDownloadFolder.Text = "Download Folder Path"
$TextBoxDownloadFolder = New-Object -TypeName System.Windows.Forms.TextBox
$TextBoxDownloadFolder.Dock = [System.Windows.Forms.DockStyle]::Fill
$ButtonDownloadFolder = New-Object -TypeName System.Windows.Forms.Button
$ButtonDownloadFolder.Text = "..."
$ButtonDownloadFolder.AutoSize = $true
$ButtonDownloadFolder.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
$FolderBrowserDownloadFolder = New-Object -TypeName System.Windows.Forms.FolderBrowserDialog
$ButtonDownloadFolder.Add_Click({ButtonDownloadFolderChooser_Click})
$TableLayoutPanelDownloadFolderSelector = New-Object -TypeName System.Windows.Forms.TableLayoutPanel
$TableLayoutPanelDownloadFolderSelector.ColumnCount = 2
$TableLayoutPanelDownloadFolderSelector.AutoSize = $true
$TableLayoutPanelDownloadFolderSelector.AutoSizeMode = [System.Windows.Forms.AutoSizeMode]::GrowAndShrink
$TableLayoutPanelDownloadFolderSelector.Dock = [System.Windows.Forms.DockStyle]::Fill
$TableLayoutPanelDownloadFolderSelector.Controls.Add($TextBoxDownloadFolder, 0, 0)
$TableLayoutPanelDownloadFolderSelector.Controls.Add($ButtonDownloadFolder, 1, 0)
$ColumnStyle = [System.Windows.Forms.ColumnStyle]::new([System.Windows.Forms.SizeType]::Percent)
$ColumnStyle.Width = 90
$TableLayoutPanelDownloadFolderSelector.ColumnStyles.Add($ColumnStyle)
$RowStyle = [System.Windows.Forms.RowStyle]::new([System.Windows.Forms.SizeType]::Absolute)

$LabelS3DownloadBucketCheckInterval = New-Object -TypeName System.Windows.Forms.Label
$LabelS3DownloadBucketCheckInterval.Text = "Download Bucket Check Interval (seconds)"

$TextBoxS3DownloadBucketCheckInterval = New-Object -TypeName System.Windows.Forms.TextBox

$ButtonSaveConfig = New-Object -TypeName System.Windows.Forms.Button
$ButtonSaveConfig.Text = "Save Config"
$ButtonSaveConfig.Anchor = [System.Windows.Forms.AnchorStyles]::Bottom, [System.Windows.Forms.AnchorStyles]::Left, [System.Windows.Forms.AnchorStyles]::Right, [System.Windows.Forms.AnchorStyles]::Top
$ButtonSaveConfig.Add_Click({SaveConfig})

$ButtonInstallAsService = New-Object -TypeName System.Windows.Forms.Button
$ButtonInstallAsService.Text = "Install PicTransfer as a service"
$ButtonInstallAsService.Anchor = [System.Windows.Forms.AnchorStyles]::Bottom, [System.Windows.Forms.AnchorStyles]::Left, [System.Windows.Forms.AnchorStyles]::Right, [System.Windows.Forms.AnchorStyles]::Top
$ButtonInstallAsService.Add_Click({ButtonInstallAsService_Click})

$ButtonStartService = New-Object -TypeName System.Windows.Forms.Button
$ButtonStartService.Text = "Start PicTransfer service"
$ButtonStartService.AutoSize = $true
$ButtonStartService.Dock = [System.Windows.Forms.DockStyle]::Fill
$ButtonStartService.Visible = $false
$ButtonStartService.Add_Click({ButtonStartService_Click})

$LabelInstalledAsService = New-Object -TypeName System.Windows.Forms.Label
$LabelInstalledAsService.Text = "Installed as service:"

$LabelInstalledAsServiceIndicator = New-Object -TypeName System.Windows.Forms.Label

$LabelServiceRunning = New-Object -TypeName System.Windows.Forms.Label
$LabelServiceRunning.Text = "Service running:"

$LabelServiceRunningIndicator = New-Object -TypeName System.Windows.Forms.Label
$LabelServiceRunningIndicator.Text = "asdf"

$StatusStrip = New-Object -TypeName System.Windows.Forms.StatusStrip

$ToolStripStatusLabel = New-Object -TypeName System.Windows.Forms.ToolStripStatusLabel
$ToolStripStatusLabel.Text = ""

#Add controls to the form
$Form.Controls.Add($TableLayoutPanel1)

$TableLayoutPanel1.Controls.Add($LabelMode, 0, 0)
$TableLayoutPanel1.Controls.Add($TableLayoutPanelRadioButtons1, 1, 0)

$TableLayoutPanel1.Controls.Add($LabelAwsKeyUpload, 0, 1)
$TableLayoutPanel1.Controls.Add($TextBoxAwsKeyUpload, 1, 1)

$TableLayoutPanel1.Controls.Add($LabelAwsSecretKeyUpload, 0, 2)
$TableLayoutPanel1.Controls.Add($TextBoxAwsSecretKeyUpload, 1, 2)

$TableLayoutPanel1.Controls.Add($LabelRegionUpload, 0, 3)
$TableLayoutPanel1.Controls.Add($TextBoxRegionUpload, 1, 3)

$TableLayoutPanel1.Controls.Add($LabelUploadBucketName, 0, 4)
$TableLayoutPanel1.Controls.Add($TextBoxUploadBucketName, 1, 4)

$TableLayoutPanel1.Controls.Add($LabelUploadFolder, 0, 5)
$TableLayoutPanel1.Controls.Add($TableLayoutPanelUploadFolderSelector, 1, 5)

$TableLayoutPanel1.Controls.Add($LabelUploadDeleteLocalFiles, 0, 6)
$TableLayoutPanel1.Controls.Add($TableLayoutpanelRadioButtonsDeleteLocalFiles, 1, 6)

$TableLayoutPanel1.Controls.Add($LabelAwsKeyDownload, 0 , 7)
$TableLayoutPanel1.Controls.Add($TextBoxAwsKeyDownload, 1, 7)

$TableLayoutPanel1.Controls.Add($LabelAwsSecretKeyDownload, 0, 8)
$TableLayoutPanel1.Controls.Add($TextBoxAwsSecretKeyDownload, 1, 8)

$TableLayoutPanel1.Controls.Add($LabelRegionDownload, 0, 9)
$TableLayoutPanel1.Controls.Add($TextBoxRegionDownload, 1, 9)

$TableLayoutPanel1.Controls.Add($LabelDownloadBucketName, 0, 10)
$TableLayoutPanel1.Controls.Add($TextBoxDownloadBucketName, 1, 10)

$TableLayoutPanel1.Controls.Add($LabelDownloadFolder, 0, 11)
$TableLayoutPanel1.Controls.Add($TableLayoutPanelDownloadFolderSelector, 1, 11)

$TableLayoutPanel1.Controls.Add($LabelS3DownloadBucketCheckInterval, 0, 12)
$TableLayoutPanel1.Controls.Add($TextBoxS3DownloadBucketCheckInterval, 1, 12)

$TableLayoutPanel1.Controls.Add($ButtonSaveConfig, 1, 13)
$TableLayoutPanel1.SetColumnSpan($ButtonSaveConfig, 2)

$TableLayoutPanel1.Controls.Add($ButtonInstallAsService, 1, 14)
$TableLayoutPanel1.SetColumnSpan($ButtonInstallAsService, 2)

$TableLayoutPanel1.Controls.Add($ButtonStartService, 1, 15)
$TableLayoutPanel1.SetColumnSpan($ButtonStartService, 2)

$TableLayoutPanel1.Controls.Add($LabelInstalledAsService, 0, 16)
$TableLayoutPanel1.Controls.Add($LabelInstalledAsServiceIndicator, 1, 16)

$TableLayoutPanel1.Controls.Add($LabelServiceRunning, 0, 17)
$TableLayoutPanel1.Controls.Add($LabelServiceRunningIndicator, 1, 17)


$StatusStrip.Items.Add($ToolStripStatusLabel)

$Form.Controls.Add($StatusStrip)



#Format all controls
foreach ( $TextBox in $($TableLayoutPanel1.Controls | Where-Object {$_ -is [System.Windows.Forms.TextBox]}) )
{
    $TextBox.Dock = [System.Windows.Forms.DockStyle]::Fill
}

foreach ( $RadioButton in $($TableLayoutPanelRadioButtons1.Controls | Where-Object {$_ -is [System.Windows.Forms.RadioButton]}) )
{
    $RadioButton.AutoSize = $true
}

foreach ( $Label in $($TableLayoutPanel1.Controls) | Where-Object {$_ -is [System.Windows.Forms.Label]} )
{
    $Label.TextAlign = [System.Drawing.ContentAlignment]::MiddleLeft
    $Label.AutoSize = $true
}

LoadConfig

#Form modification that need to be made as the form loads
$Form.Add_Load({
    CheckServiceInstalled
    CheckServiceRunning
})

#[System.Windows.Forms.Application]::EnableVisualStyles()
[System.Windows.Forms.Application]::Run($Form)